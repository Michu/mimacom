import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { RootState } from './store'

import { CartItem } from '../common'

export interface CartState {
    cart: Array<CartItem>
}

interface ChangeQuantityPayload {
    id: string
    qty: number
}

const initialState: CartState = {
    cart: []
}

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addProduct: (state, action: PayloadAction<CartItem>) => {
            const cartItemIndex = state.cart.findIndex((cartItem: CartItem) => cartItem.id === action.payload.id)

            if(cartItemIndex !== -1) {
                const alreadyAddedItem = state.cart[cartItemIndex]

                // @NOTE We can 'mutate' cart because of redux-toolkit implementation.
                // ¡¡¡¡¡DON'T MUTATE STATE if you are not using redux-toolkit!!!!!
                state.cart[cartItemIndex] = { ...alreadyAddedItem, qty: alreadyAddedItem.qty + action.payload.qty }

                return
            }  

            state.cart = [...state.cart, action.payload]
        },
        deleteProduct: (state, action: PayloadAction<string>) => {
            const cartItemIndex = state.cart.findIndex((cartItem: CartItem) => cartItem.id === action.payload)

            if(cartItemIndex !== -1) {
                state.cart = [ ...state.cart.slice(0, cartItemIndex), ...state.cart.slice(cartItemIndex + 1)]
            }
        },
        changeQuantity: (state, action: PayloadAction<ChangeQuantityPayload>) => {
            state.cart = state.cart.map((cartItem: CartItem) => {
                if (cartItem.id !== action.payload.id) {
                    return cartItem
                }

                return { ...cartItem, qty: action.payload.qty }
            })
        }
    }
})

export const { addProduct, deleteProduct, changeQuantity } = cartSlice.actions

export const selectCart = (state: RootState) => state.cart.cart

export default cartSlice.reducer
