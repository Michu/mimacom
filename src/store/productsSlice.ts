import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

import { RootState, AppThunk } from './store'

import {
    fetchFavourites,
    fetchProducts,
    patchProduct
} from '../common/services/ProductsService'

import { Product } from '../common'

export interface ProductsState {
    mainProducts: Array<Product>
    favouriteProducts: Array<Product>
}

const initialState: ProductsState = {
    mainProducts: [],
    favouriteProducts: []
}

export const getProductsAsync = createAsyncThunk(
    'products/fetch',
    async () => {
        const response = await fetchProducts()

        return response
    }
)

export const getFavouritesAsync = createAsyncThunk(
    'products/fetch-favourites',
    async () => {
        const response = await fetchFavourites()

        return response
    }
)

export const toggleFavouriteAsync = (data: any): AppThunk => (
    dispatch
) => {
    patchProduct(data.id, data.body).then(() => {
        dispatch(toggleFavourite(data.id))
        dispatch(getFavouritesAsync())
    })
}

export const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        toggleFavourite: (state, action: PayloadAction<string>) => {
            const productIndex = state.mainProducts.findIndex((product: Product) => product.id === action.payload)

            if(productIndex === -1) {
                return
            }

            state.mainProducts[productIndex].favorite = !state.mainProducts[productIndex].favorite ? 1 : 0
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getProductsAsync.fulfilled, (state, action) => {
                state.mainProducts = action.payload.items
            })
            .addCase(getFavouritesAsync.fulfilled, (state, action) => {
                state.favouriteProducts = action.payload.items
            })
    },
})

export const { toggleFavourite } = productsSlice.actions

export const selectProducts = (state: RootState) => state.products.mainProducts

export const selectFavouriteProducts = (state: RootState) => state.products.favouriteProducts

export default productsSlice.reducer
