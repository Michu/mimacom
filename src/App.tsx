import React from 'react'
import {
  Switch,
  Route
} from 'react-router-dom'

import Hidden from '@material-ui/core/Hidden'

import { ProductListContainer } from './product-list'
import { CartContainer } from './cart'
import { Header, MobileFooter } from './common'

import './App.css'

export const FAVS_LOCATION_PATH = '/favs'

function App() {
  return (<>
    <Hidden smDown>
      <Header />
    </Hidden>
      <div className='App'>
        <Switch>
          <Route path={['/', FAVS_LOCATION_PATH]} exact>
            <ProductListContainer />
          </Route>
          <Route path="/cart" exact>
            <CartContainer />
          </Route>
          <Route path="*">
            {<h1>404 Not Found =/</h1>}
          </Route>
        </Switch>
      </div>
    <Hidden mdUp>
      <MobileFooter />
    </Hidden>
  </>)
}

export default App
