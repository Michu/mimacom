import React from 'react'
import { useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'

import { addProduct } from '../../store/cartSlice'

import Grid from '@material-ui/core/Grid'
import Snackbar from '@material-ui/core/Snackbar'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import { ProductItem } from './ProductItem'

import { Product } from '../../common'

interface ProductListProps { productList: Array<Product> }

const useStyles = makeStyles((theme) => ({
    toastText: {
        paddingLeft: theme.spacing(8),
        paddingRight: theme.spacing(8),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2)
    }
}))

export const ProductList = ({ productList }: ProductListProps) => {
    const dispatch = useDispatch()
    const [showToast, setShowToast] = React.useState(false)
    const classes = useStyles()

    if (!productList) {
        return <p>No data to show</p>
    }

    const onAddProduct = (product: Product) => {
        if (product.stock === 0) {
            return false
        }

        // @NOTE: If we add a quantity selector replace hardcoded '1' by quantity in params
        dispatch(addProduct({ ...product, qty: 1 }))

        // Show toast on success add
        setShowToast(true)
    }

    const handleCloseToast = () => {
        setShowToast(false)
    }

    const products = productList.map((product: Product) => (
        <Grid key={product.id} item xs={6} sm={4} md={3} lg={2}>
            <ProductItem product={product} onAddProduct={onAddProduct} />
        </Grid>
    ))

    return (<>
        <Grid container spacing={3} alignContent={'center'} >{products}</Grid>
        <Snackbar open={showToast} autoHideDuration={1500} onClose={handleCloseToast}>
            <Paper>
                <Typography className={classes.toastText} variant="h5">
                    Product added!
                </Typography>
            </Paper>
        </Snackbar>
    </>)
}