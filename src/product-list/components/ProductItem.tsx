import React from 'react'
import { useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'

import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart'
import FavoriteIcon from '@material-ui/icons/Favorite'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder'
import IconButton from '@material-ui/core/IconButton'

import { Product } from '../../common'
import { toggleFavouriteAsync } from '../../store/productsSlice'

interface ProductProps { product: Product, onAddProduct: Function }

const useStyles = makeStyles(() => ({
    disabledCart: {
        color: 'darkgrey',
        cursor: 'not-allowed'
    },
    action: {
        fontSize: '2rem',
        padding: '0.5rem'
    }
}))

export const ProductItem = ({ product, onAddProduct }: ProductProps) => {
    const classes = useStyles()
    const dispatch = useDispatch()

    const onCartClick = () => {
        onAddProduct(product)
    }

    const toggleFav = () => {
        const body = { favorite: !product.favorite ? 1 : 0 }

        dispatch(toggleFavouriteAsync({ id: product.id, body }))
    }

    return (
        <Card raised>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt={product.productName}
                    height="225"
                    image={product.image_url}
                />
                <CardContent>
                    <Typography variant="h6" noWrap>
                        {product.productName}
                    </Typography>
                    <hr />
                    <Typography variant="body2" color="textSecondary" align="justify" noWrap>
                        {product.productDescription}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                >
                    <Grid item xs={12} sm={12} md={6} lg={4}>
                        <Typography variant="h5">
                            Precio: {product.price}€
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={4}>
                        <Typography variant="body2">
                            Units available: {product.stock}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={4}>
                        <IconButton onClick={toggleFav}>
                            {product.favorite === 1 ? <FavoriteIcon className={classes.action} /> : <FavoriteBorderIcon className={classes.action}/>}
                        </IconButton>
                        <IconButton onClick={onCartClick} >
                            <AddShoppingCartIcon className={product.stock ? classes.action : [classes.action, classes.disabledCart].join(' ')} />
                        </IconButton>
                    </Grid>
                </Grid>
            </CardActions>
        </Card >
    )
}