import React, {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from "react-router-dom"

import {
    getFavouritesAsync,
    getProductsAsync,
    selectProducts,
    selectFavouriteProducts
} from '../../store/productsSlice'

import { ProductList } from '../'

import { FAVS_LOCATION_PATH } from '../../App'

export const ProductListContainer = () => {
    const dispatch = useDispatch()
    const location = useLocation()
    const isFavPath = location.pathname === FAVS_LOCATION_PATH

    const products = useSelector(selectProducts)
    const favouriteProducts = useSelector(selectFavouriteProducts)

    useEffect(() => {
        const actionToDispatch = isFavPath ? getFavouritesAsync() : getProductsAsync()

        dispatch(actionToDispatch)
    }, [isFavPath, dispatch])

    return (<>
        <h1>{ isFavPath ? 'Favourites List' : 'Main List' }</h1>
        <ProductList productList={isFavPath ? favouriteProducts : products} />
    </>)
}