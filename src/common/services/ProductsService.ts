import parse from 'parse-link-header'

import { Product } from '../model'

const HTTP_HOST = 'http://localhost:8080'

export function fetchProducts() {
    return fetch(`${HTTP_HOST}/grocery?_page=1`).then(async (res) => {
        const body: Array<Product> = await res.json()
        const linkHeader = res.headers.get('Link') || ''

        return { links: parse(linkHeader), items: body }
    })
} 
export function fetchFavourites() {
    return fetch(`${HTTP_HOST}/grocery?favorite=1&_page=1`).then(async (res) => {
        const body: Array<Product> = await res.json()
        const linkHeader = res.headers.get('Link') || ''

        return { links: parse(linkHeader), items: body }
    })
} 

export function patchProduct(id: string, modifiedBody: any) {
    return fetch(
        `${HTTP_HOST}/grocery/${id}`,
        {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(modifiedBody)
    }).then(async (res) => {
        return await res.json()
    })
} 