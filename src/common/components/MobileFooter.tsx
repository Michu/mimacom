import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory } from 'react-router-dom'

import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction'
import FavoriteIcon from '@material-ui/icons/Favorite'
import HomeIcon from '@material-ui/icons/Home'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'

import { ROUTES_BY_OPTION } from './Header'

const useStyles = makeStyles({
    root: {
        backgroundColor: 'aquamarine',
        position: 'fixed',
        left: '0',
        bottom: '0',
        width: '100%'
    },
})

export const MobileFooter = () => {
    const classes = useStyles()
    const history = useHistory()

    const [selectedOption, setSelectedOption] = React.useState(0)

    const handleMenuClick = (event: any, seletedOption: number) => {
        history.push(ROUTES_BY_OPTION[seletedOption])

        setSelectedOption(seletedOption)
    }

    return (
        <BottomNavigation
            value={selectedOption}
            onChange={handleMenuClick}
            showLabels
            className={classes.root}
        >
            <BottomNavigationAction label="Home" icon={<HomeIcon />} />
            <BottomNavigationAction label="Favorites" icon={<FavoriteIcon />} />
            <BottomNavigationAction label="Cart" icon={<ShoppingCartIcon />} />
        </BottomNavigation>
    )
}
