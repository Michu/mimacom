import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory } from 'react-router-dom'

import Button from '@material-ui/core/Button'
import FavoriteIcon from '@material-ui/icons/Favorite'
import HomeIcon from '@material-ui/icons/Home'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'

const useStyles = makeStyles({
    navbar: {
        padding: '1rem',
        alignItems: 'center',
        display: 'flex',
        backgroundColor: 'aquamarine',
        width: '100%'
    }
})

export const ROUTES_BY_OPTION = [
    '/',
    '/favs',
    '/cart'
]

export const Header = () => {
    const classes = useStyles()
    const history = useHistory()

    const handleMenuClick = (seletedOption: number) => {
        history.push(ROUTES_BY_OPTION[seletedOption])
    }

    return (
        <div className={classes.navbar}>
            <Button onClick={() => handleMenuClick(0)} startIcon={<HomeIcon />}>Home</Button>
            <Button onClick={() => handleMenuClick(1)} startIcon={<FavoriteIcon />}>Favs</Button>
            <Button onClick={() => handleMenuClick(2)} startIcon={<ShoppingCartIcon />}>Cart</Button>
        </div>
    )
}
