import React from 'react'
import { useSelector } from 'react-redux'

import { CartLine } from './CartLine'

import { CartItem } from '../../common'

import { selectCart } from '../../store/cartSlice'  

export const Cart = () => {
    const cart = useSelector(selectCart)

    const cartLines = cart.map((cartItem: CartItem) => (
        <CartLine key={cartItem.id} cartItem={cartItem}/>
    ))

    return (<>
        <h1>Cart</h1>
        {cartLines}
    </>)
}