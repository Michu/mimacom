import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useDispatch } from 'react-redux'

import { changeQuantity, deleteProduct } from '../../store/cartSlice'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'
import TextField from '@material-ui/core/TextField'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'

import { CartItem } from '../../common'

interface CartLineProps { cartItem: CartItem }

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    productData: {
        display: 'flex',
        flexDirection: 'column'
    },
    productImage: {
        width: 150
    },
    qtySelector: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1)
    },
    qtyInput: {
        height: 38,
        width: 38
    },
}))

export const CartLine = ({ cartItem }: CartLineProps) => {
    const classes = useStyles()
    const dispatch = useDispatch()

    const hadleMinusClick = () => {
        const newQty = cartItem.qty - 1

        if (!newQty) {
            dispatch(deleteProduct(cartItem.id))
            
            return
        }

        dispatch(changeQuantity({ qty: newQty, id: cartItem.id }))
    }

    const handlePlusClick = () => {
        dispatch(changeQuantity({ qty: cartItem.qty + 1, id: cartItem.id }))
    }

    return (
        <Card className={classes.root} raised>
            <div className={classes.productData}>
                <CardContent>
                    <Typography component="h6" variant="h6">
                        {cartItem.productName}
                    </Typography>
                </CardContent>
                <div className={classes.qtySelector}>
                    <IconButton onClick={hadleMinusClick}>
                        {cartItem.qty === 1 ? <DeleteForeverIcon /> : <RemoveIcon />}
                    </IconButton>
                    <TextField
                        className={classes.qtyInput}
                        label="Qty"
                        type="number"
                        InputLabelProps={{
                            shrink: true
                        }}
                        value={cartItem.qty}
                        disabled

                    />
                    <IconButton onClick={handlePlusClick} disabled={cartItem.qty >= cartItem.stock}>
                        <AddIcon />
                    </IconButton>
                    <div>
                        Subtotal: { cartItem.qty * cartItem.price }€
                    </div>
                </div>
            </div>
            <CardMedia
                component="img"
                className={classes.productImage}
                image={cartItem.image_url}
                alt={cartItem.productName}
            />
        </Card>
    )
}